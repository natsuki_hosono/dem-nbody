#pragma once

class Unit{
	const double M;
	const double L;
	//const double T;
	static const double G = 6.67e-11;
	public:
	Unit(const double M_, const double L_): M(M_), L(L_){
	}
	double getUnitTime() const{
		return sqrt(pow(L, 3) / (G * M));
	}
	double getUnitDensity() const{
		return M / pow(L, 3);
	}
	double getUnitPressure() const{
		return M / (L * getUnitTime() * getUnitTime());
	}
};

class FileHeader{
	public:
	int Nbody;
	double time;
	int readAscii(FILE* fp){
		fscanf(fp, "%lf\n", &time);
		fscanf(fp, "%d\n", &Nbody);
		return Nbody;
	}
	void writeAscii(FILE* fp) const{
		fprintf(fp, "%e\n", time);
		fprintf(fp, "%d\n", Nbody);
	}
};

class FP{
	public:
	//id
	PS::S64    id;
	//tag
	PS::S64    tag;
	//mass
	PS::F64    mass;
	//inertia
	PS::F64    iner;
	//position & angle
	PS::F64vec pos;
	PS::F64vec ang;
	//velocity
	PS::F64vec vel, vel_half;
	PS::F64vec avel, avel_half;
	//acceleration
	PS::F64vec acc;
	//torque
	PS::F64vec tor;
	//radius
	PS::F64    rad;
	//timestep
	PS::F64    dt;

	PS::F64vec getPos() const{
		return pos;
	}
	PS::F64 getCharge() const{
		return mass;
	}
	PS::F64 getRSearch() const{
		return 2.0 * rad;
	}
	void setPos(const PS::F64vec& pos){
		this->pos = pos;
	}
	void copyFromForce(const HardSphere& diff){
		this->pos  += diff.pos;
		this->vel  += diff.vel;
		this->avel += diff.avel;
	}
	void copyFromForce(const Gravity& force){
		this->acc += force.acc;
	}
	void copyFromFP(const FP& fp){
		*this = fp;
	}
	void writeAscii(FILE* fp) const{
		//fprintf(fp, "%lld\t%lld\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n", id, tag, mass, iner, rad, pos.x, pos.y, pos.z, vel.x, vel.y, vel.z, ang.x, ang.y, ang.z);
		fprintf(fp, "%lld\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n", id, mass, rad, pos.x, pos.y, pos.z, vel.x, vel.y, vel.z);
	}
	//Euler
	void update(const PS::F64 dt){
		pos += vel * dt;
		ang += avel * dt;
		vel += acc * dt;
		avel += tor * dt;
	}
	//KDK
	void kick(const PS::F64 dt){
		vel_half = vel + 0.5 * acc * dt;
		avel_half = avel + 0.5 * tor * dt;
	}
	void drift(const PS::F64 dt){
		pos += vel_half * dt;
		vel += acc * dt;
		avel += tor * dt;
	}
	void kick2(const PS::F64 dt){
		vel = vel_half + 0.5 * acc * dt;
		avel = avel_half + 0.5 * tor * dt;
	}
	//
	void clear(){
		acc = 0.0;
		tor = 0.0;
	}
};

struct system_t{
	PS::F64 time, dt, end_time;
	unsigned int step;
	system_t() : time(0.0), step(0), end_time(0.0), dt(1.0e+30){
	}
};


