#pragma once

template <class ThisPtcl> class Problem{
	public:
	static void SetIC(PS::ParticleSystem<ThisPtcl>& ptcl, PS::DomainInfo& dinfo, system_t& sysinfo){
		ptcl.setNumberOfParticleLocal(1);
		//dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XYZ);
		//dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0, 0.0), PS::F64vec(1.0, 1.0, 1.0));
		sysinfo.end_time = 5.0;
		if(PS::Comm::getRank() != 0) return;
		ptcl[0].id = 0;
		ptcl[0].pos.x = 0.0;
		ptcl[0].pos.y = 0.0;
		ptcl[0].pos.z = 1.0;
		ptcl[0].vel.x = 0.0;
		ptcl[0].vel.y = 0.0;
		ptcl[0].vel.z = 0.0;
		ptcl[0].rad = 0.1;
		ptcl[0].mat = Basalt;
		ptcl[0].mass = 4.0 * M_PI / 3.0 * pow(ptcl[0].rad, 3) * ptcl[0].mat.getDensity();
		/*
		ptcl[1].id = 1;
		ptcl[1].pos.x = 0.0;
		ptcl[1].pos.y = 0.0;
		ptcl[1].pos.z = 2.0;
		ptcl[1].vel.x = 0.0;
		ptcl[1].vel.y = 0.0;
		ptcl[1].vel.z = -1.0;
		ptcl[1].rad = 0.1;
		ptcl[1].mat = Ice;
		ptcl[1].mass = 4.0 * M_PI / 3.0 * pow(ptcl[1].rad, 3) * ptcl[1].mat.getDensity();
		*/
	}
	static void externalForce(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){

		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			//Earth's gravity
			ptcl[i].acc.z -= 9.8;
			//contact force between wall at z = 0;
			const PS::F64vec dr = - PS::F64vec(0.0, 0.0, ptcl[i].pos.z);
			const PS::F64vec dv = - PS::F64vec(0.0, 0.0, ptcl[i].vel.z);
			const PS::F64    pd = ptcl[i].rad - std::abs(dr.z);
			#if 1
			const PS::F64vec n = PS::F64vec(0.0, 0.0, 1.0);
			const Material mat = Basalt;
			const PS::F64 Eij = 1.0 / ((1.0 - pow(ptcl[i].mat.getPoissonRatio(), 2)) / ptcl[i].mat.getYoungModulus() + (1.0 - pow(mat.getPoissonRatio(), 2)) / mat.getYoungModulus());
			const PS::F64 rij = ptcl[i].rad;
			const PS::F64 mij = ptcl[i].mass;
			const PS::F64 knr = 4.0 / 3.0 * Eij * sqrt(rij);
			const PS::F64vec Fnr = knr * powf(pd, 1.5) * n;
			const PS::F64vec Fnd = 0.3 * sqrt(6.0 * mij * Eij * sqrt(rij * pd)) * (dv * n) * n;
			if(pd < 0) continue;
			//ptcl[i].dt  = std::min(M_PI / 50.0 * sqrt(mij / knr), ptcl[i].dt);
			ptcl[i].acc += (Fnr + Fnd) / ptcl[i].mass;
			#else
			if(pd < 0) continue;
			ptcl[i].vel.z = - 0.1 * ptcl[i].vel.z;
			ptcl[i].vel_pred.z = - 0.1 * ptcl[i].vel_pred.z;
			ptcl[i].pos.z = 1.01 * ptcl[i].rad;
			//getchar();
			#endif
		}
	}
};

