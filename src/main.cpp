#include <particle_simulator.hpp>

#include "force.h"
#include "class.h"
#include "surface.h"
#include "init/Moon.h"
#include "integral.h"
#include "io.h"
#ifdef ENABLE_PEZY
#include "pezycl.h"
#endif

int main(int argc, char *argv[]){
	PS::Initialize(argc, argv);
	PS::ParticleSystem<FP> ptcl;
	ptcl.initialize();
	#ifdef ENABLE_PEZY
	omp_set_num_threads(4);
	extern PezyDevice device;
	device.initialize();
	#endif
	PS::TreeForForceShort<Force, FP, FP>::Symmetry tree_coll;
	PS::TreeForForceShort<HardSphere, FP, FP>::Symmetry tree_HardSphere;
	PS::TreeForForceLong <Gravity, FP, FP>::Monopole grav_tree;

	PS::DomainInfo dinfo;
	dinfo.initialize();
	system_t sysinfo;
	FileIO<FP> io;

	std::cout << argc << std::endl;
	if(argc == 1){
		Problem<FP>::SetIC(ptcl, dinfo, sysinfo);
		tree_HardSphere.initialize(ptcl.getNumberOfParticleLocal());
		tree_coll.initialize(ptcl.getNumberOfParticleLocal());
		grav_tree.initialize(ptcl.getNumberOfParticleLocal(), 0.5, 16, 128);
	}else{
		io.Restore(ptcl, sysinfo);
		tree_HardSphere.initialize(ptcl.getNumberOfParticleLocal());
		tree_coll.initialize(ptcl.getNumberOfParticleLocal());
		grav_tree.initialize(ptcl.getNumberOfParticleLocal(), 0.5, 16, 128);
		goto savepoint;
	}
	/*
	tree_HardSphere.initialize(ptcl.getNumberOfParticleLocal());
	tree_coll.initialize(ptcl.getNumberOfParticleLocal());
	grav_tree.initialize(ptcl.getNumberOfParticleLocal(), 0.5, 16, 128);
	*/

	dinfo.decomposeDomainAll(ptcl);
	ptcl.exchangeParticle(dinfo);
	tree_HardSphere.calcForceAllAndWriteBack(HardSphere(), ptcl, dinfo);
	//tree_coll.calcForceAllAndWriteBack(Force(), ptcl, dinfo);
	#ifdef ENABLE_PEZY
		grav_tree.calcForceAllAndWriteBackMultiWalk(DispatchKernelWithSP, RetrieveKernel, 1, ptcl, dinfo, N_WALK_LIMIT);
	#else
		grav_tree.calcForceAllAndWriteBack(Gravity(), Gravity(), ptcl, dinfo);
	#endif
	Problem<FP>::externalForce(ptcl, sysinfo);

	for(sysinfo.time = 0, sysinfo.step = 0 ; sysinfo.time < sysinfo.end_time ; sysinfo.time += sysinfo.dt, ++ sysinfo.step){
		dinfo.decomposeDomainAll(ptcl);
		ptcl.exchangeParticle(dinfo);
		sysinfo.dt = GetGlobalTimestep<FP>(ptcl);
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			ptcl[i].kick(sysinfo.dt);
			ptcl[i].drift(sysinfo.dt);
			ptcl[i].clear();
		}
		//tree_coll.calcForceAllAndWriteBack(Force(), ptcl, dinfo);
		grav_tree.clearTimeProfile();
		//const PS::F64 st = PS::GetWtime();
		#ifdef ENABLE_PEZY
			grav_tree.calcForceAllAndWriteBackMultiWalk(DispatchKernelWithSP, RetrieveKernel, 1, ptcl, dinfo, N_WALK_LIMIT);
		#else
			grav_tree.calcForceAllAndWriteBack(Gravity(), Gravity(), ptcl, dinfo);
		#endif
		//std::cout << grav_tree.getTimeProfile().getTotalTime() << std::endl;
		//grav_tree.getTimeProfile().dump();
		//std::cout << PS::GetWtime() - st << std::endl;
		Problem<FP>::externalForce(ptcl, sysinfo);
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			ptcl[i].kick2(sysinfo.dt);
		}
		//for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i) ptcl[i].update(sysinfo.dt);
		tree_HardSphere.calcForceAllAndWriteBack(HardSphere(), ptcl, dinfo);
		Problem<FP>::postTimestep(ptcl, sysinfo);
		ptcl.adjustPositionIntoRootDomain(dinfo);
		if(PS::Comm::getRank() == 0 && sysinfo.step % 100 == 0) std::cout << "time = " << sysinfo.time << " (dt = " << sysinfo.dt << ")" << std::endl;
		io.OutputFileWithTimeInterval(ptcl, sysinfo);
		if(sysinfo.step % 100 == 0) io.Create(ptcl, sysinfo);
		savepoint:;
	}

	
	PS::Finalize();
	return 0;
}

