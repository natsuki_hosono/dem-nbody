#pragma once
class HardSphere{
	static const PS::F64 eps_n = 0.1;
	static const PS::F64 eps_t = 1.0;
	public:
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64vec avel;
	void clear(){
		pos = vel = 0;
	}
	template <class ThisPtcl> void operator () (const ThisPtcl* const ep_i, const PS::S32 Nip, const ThisPtcl* const ep_j, const PS::S32 Njp, HardSphere* const diff){
		for(PS::S32 i = 0 ; i < Nip ; ++ i){
			const ThisPtcl& ith = ep_i[i];
			for(PS::S32 j = 0 ; j < Njp ; ++ j){
				const ThisPtcl& jth = ep_j[j];
				const PS::F64vec dr = jth.pos - ith.pos;
				const PS::F64vec dv = jth.vel - ith.vel + jth.avel * jth.rad - ith.avel * ith.rad;
				const PS::F64    pd = ith.rad + jth.rad - sqrt(dr * dr);
				if(pd < 0 || dr * dr <= 0.0) continue;
				const PS::F64vec n = dr / sqrt(dr * dr);
				diff[i].pos += - jth.mass / (ith.mass + jth.mass) * pd * n;
				diff[i].vel += jth.mass / (ith.mass + jth.mass) * ((1.0 + eps_n) * (dv * n) * n + 2./7. * (1.0 - eps_t) * (dv - (dv * n) * n));
				diff[i].avel += 2./7. * ith.mass * jth.mass / (ith.mass + jth.mass) * (1.0 - eps_t) * ((ith.rad * n) ^ dv);
			}
		}
	}
};

class Force{
	static const PS::F64 cn = 0.3;
	public:
	PS::F64vec acc;
	PS::F64vec tor;
	PS::F64 dt;
	void clear(){
		acc = tor = 0;
		dt = 1.0e+30;
	}
	template <class ThisPtcl> void operator () (const ThisPtcl* const ep_i, const PS::S32 Nip, const ThisPtcl* const ep_j, const PS::S32 Njp, Force* const force){
		for(PS::S32 i = 0 ; i < Nip ; ++ i){
			const ThisPtcl& ith = ep_i[i];
			force[i].dt = M_PI / 50.0 * sqrt(ith.mass / (2.0 / 3.0 * ith.mat.getYoungModulus() * sqrt(ith.rad * 0.5)));
			for(PS::S32 j = 0 ; j < Njp ; ++ j){
				const ThisPtcl& jth = ep_j[j];
				const PS::F64vec dr = jth.pos - ith.pos;
				const PS::F64vec dv = jth.vel - ith.vel;
				const PS::F64    pd = ith.rad + jth.rad - sqrt(dr * dr);
				if(pd < 0 || dr * dr <= 0.0) continue;
				const PS::F64vec n = dr / sqrt(dr * dr);

				const PS::F64 Eij = 1.0 / ((1.0 - pow(ith.mat.getPoissonRatio(), 2)) / ith.mat.getYoungModulus() + (1.0 - pow(jth.mat.getPoissonRatio(), 2)) / jth.mat.getYoungModulus());
				const PS::F64 rij = 1.0 / (1.0 / ith.rad + 1.0 / jth.rad);
				const PS::F64 mij = 1.0 / (1.0 / ith.mass + 1.0 / jth.mass);
				//replusive normal force
				const PS::F64 knr = 4.0 / 3.0 * Eij * sqrt(rij);
				const PS::F64vec Fnr = - knr * powf(pd, 1.5) * n;
				//damp normal force
				const PS::F64 Cn = cn * sqrt(6.0 * mij * Eij * sqrt(rij * pd));
				const PS::F64vec Fnd = Cn * (dv * n) * n;
				force[i].acc += Fnr + Fnd;
				force[i].dt  = std::min(M_PI / 50.0 * sqrt(mij / (knr * (1.0 - Cn * Cn / (knr * knr)))), force[i].dt);
			}
			force[i].acc /= ith.mass;
			force[i].tor /= ith.mass;
		}
	}
};

class Gravity{
	//static const PS::F64 G = 6.67e-11;
	static const PS::F64 G = 1.0;
	public:
	PS::F64vec acc;
	PS::F64 pot;
	PS::F64 dt;
	float fast_rsqrt(const float x) {
		int i = *(int*)&x;
		i = 0x5f3759df - (i >> 1);
		const float y = *(float*)&i;
		return y * (1.5f - 0.5f * x * y * y);
	}
	void clear(){
		acc = 0;
		pot = 0;
		dt = 1.0e+30;
	}
	template <class ThisPtcl, class ThatPtcl> void operator () (const ThisPtcl* const ep_i, const PS::S32 Nip, const ThatPtcl* const ep_j, const PS::S32 Njp, Gravity* const force){
		for(PS::S32 i = 0 ; i < Nip ; ++ i){
			const ThisPtcl& ith = ep_i[i];
			for(PS::S32 j = 0 ; j < Njp ; ++ j){
				const ThatPtcl& jth = ep_j[j];
				const PS::F64vec dr = jth.pos - ith.pos;
				const PS::F64 dr2 = dr * dr;
				const PS::F64 dr_inv = 1.0 / sqrt(dr2 + 1.0e-6);
				const PS::F64 m_dr3_inv = jth.mass * pow(dr_inv, 3);
				force[i].acc += m_dr3_inv * dr;
				force[i].pot += jth.mass * dr_inv;
			}
			force[i].acc *= G;
			force[i].pot *= G;
		}
	}
};

