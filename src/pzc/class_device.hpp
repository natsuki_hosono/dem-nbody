typedef float real;

struct EpiDev{
	real rx;
	real ry;
	real rz;
	int id_walk;
};

struct EpjDev{
	real rx;
	real ry;
	real rz;
	real mass;
};
struct ForceDev{
	real ax;
	real ay;
	real az;
	real pot;
};

