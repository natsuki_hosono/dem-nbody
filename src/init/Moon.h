#pragma once

template <class ThisPtcl> class CartesianToOrbitalElement{
	//Gravity constant
	static const double G = 1.0;
	//mass of the central object
	static const double M = 1.0;
	public:
	CartesianToOrbitalElement(){
		std::cout << "Warning!: This class assumes the mass of the cantral object x G = 1.0 and it lies at the origin." << std::endl;
	}
	static double getSemimajorAxis(const ThisPtcl ptcl){
		const double mu = G * (M + ptcl.mass);
		return 1.0 / (2.0 / sqrt(ptcl.pos * ptcl.pos) - ptcl.vel * ptcl.vel / mu);
	}
	static double getEccentricity(const ThisPtcl ptcl){
		const double mu = G * (M + ptcl.mass);
		const double r = sqrt(ptcl.pos * ptcl.pos);
		const double a = getSemimajorAxis(ptcl);
		const double rv = ptcl.pos * ptcl.vel;
		return sqrt(pow(1.0 - r / a, 2) + rv * rv / (mu * a));
	}
};

template <class ThisPtcl> ThisPtcl OrbitalElementToCartesian(const double a, const double e, const double i, const double Omega, const double omega, const double E){
	//Gravity constant
	static const double G = 1.0;
	//mass of the central object
	static const double M = 1.0;

	ThisPtcl ith;
	const PS::F64vec P = PS::F64vec(  cos(omega) * cos(Omega) - sin(omega) * sin(Omega) * cos(i),   cos(omega) * sin(Omega) + sin(omega) * cos(Omega) * cos(i), sin(omega) * sin(i));
	const PS::F64vec Q = PS::F64vec(- sin(omega) * cos(Omega) - cos(omega) * sin(Omega) * cos(i), - sin(omega) * sin(Omega) + cos(omega) * cos(Omega) * cos(i), cos(omega) * sin(i));
	//const PS::F64vec R = PS::F64vec(                                         sin(Omega) * sin(i),                                        - cos(Omega) * sin(i),              cos(i));
	const double an = sqrt(G * (M + ith.mass) / a);
	ith.pos = a * (cos(E) - e) * P + a * sqrt(1.0 - e * e) * sin(E) * Q;
	ith.vel = an / sqrt(ith.pos * ith.pos) * a * (- sin(E) * P + sqrt(1.0 - e * e) * cos(E) * Q);
	return ith;
}


template <class ThisPtcl> class Problem{
	static double getRandom(){
		return (double)rand() / (double)RAND_MAX;
	}
	public:
	static void SetIC(PS::ParticleSystem<ThisPtcl>& ptcl, PS::DomainInfo& dinfo, system_t& sysinfo){
		ptcl.setNumberOfParticleLocal(0);
		sysinfo.end_time = 1.0e+3;
		if(PS::Comm::getRank() != 0) return;
		#if 1
		const int N = 1e+4;
		const double Mdisc = 4.0 * 0.0123;
		const double q    = 3.0;
		const double rin  = 1.0;
		const double rout = 2.9;
		ptcl.setNumberOfParticleLocal(N);

		for(std::size_t j = 0 ; j < N ; ++ j){
			ThisPtcl ith;
			//const double a = rin + (rout - rin) * getRandom();
			const double a = powf((powf(rout, -(q - 2.0)) - powf(rin, -(q - 2.0))) * getRandom() + powf(rin, - (q - 2.0)), - 1.0  /(q - 2.0));
			const double e = 0.3 * sqrt(-1.0 * log(getRandom()));
			const double i = 0.15 * sqrt(-1.0 * log(getRandom()));
			const double E = getRandom() * 2.0 * M_PI;
			const double omega = getRandom() * 2.0 * M_PI;
			const double Omega = getRandom() * 2.0 * M_PI;
			if (e > 1.0 || i > 1.0){
				j --;
				continue;
			}
			ith = OrbitalElementToCartesian<ThisPtcl>(a, e, i, Omega, omega, E);
			ith.mass = Mdisc / (double)N;
			ith.rad  = powf(ith.mass, 1.0/3.0) * powf(3.3/5.5, -1.0/3.0);
			if(sqrt(ith.pos * ith.pos) < 1.0){
				j --;
				continue;
			}
			ptcl[j] = ith;
			//std::cout << ith.pos << std::endl;
		}
		#else
		/*
		Unit EM(6.0e+24, 6400e+3);
		std::cout << EM.getUnitTime() << std::endl;
		std::cout << EM.getUnitDensity() << std::endl;
		std::cout << EM.getUnitPressure() << std::endl;
		*/
		sysinfo.end_time = 1.0e+3;
		double time;
		int N;
		std::cin >> time;
		std::cin >> N;
		ptcl.setNumberOfParticleLocal(N);
		std::cout << N << std::endl;
		int cnt = 0;
		while(!std::cin.eof()){
			ThisPtcl ith;
			std::cin >> ith.id;
			std::cin >> ith.mass;
			std::cin >> ith.rad;
			std::cin >> ith.pos.x;
			std::cin >> ith.pos.y;
			std::cin >> ith.pos.z;
			std::cin >> ith.vel.x;
			std::cin >> ith.vel.y;
			std::cin >> ith.vel.z;
			if (ith.id == 0) continue;
			ptcl[cnt] = ith;
			cnt ++;
		}
		#endif
	}
	static void externalForce(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			//centrifugal force
			ptcl[i].acc -= ptcl[i].pos / pow(ptcl[i].pos * ptcl[i].pos, 1.5);
			//modify  Timestep to fixed value
			ptcl[i].dt = 1.0/256.0;
		}
	}
	static void postTimestep(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			if(sqrt(ptcl[i].pos * ptcl[i].pos) < (1.0 + ptcl[i].rad) && ptcl[i].mass < 0.9){
				//std::cout << "kill" << std::endl;
				ptcl[i] = ptcl[ptcl.getNumberOfParticleLocal() - 1];
				ptcl.setNumberOfParticleLocal(ptcl.getNumberOfParticleLocal() - 1);
				i --;
			}
		}
	}
};

