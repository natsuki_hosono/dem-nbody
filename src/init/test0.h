#pragma once

template <class ThisPtcl> class Problem{
	public:
	static void SetIC(PS::ParticleSystem<ThisPtcl>& ptcl, PS::DomainInfo& dinfo, system_t& sysinfo){
		ptcl.setNumberOfParticleLocal(2);
		//dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XYZ);
		//dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0, 0.0), PS::F64vec(1.0, 1.0, 1.0));
		sysinfo.end_time = 0.5;
		if(PS::Comm::getRank() != 0) return;
		ptcl[0].id = 0;
		ptcl[0].pos.x = 0.0;
		ptcl[0].pos.y = 0.0;
		ptcl[0].pos.z = 0.0;
		ptcl[0].vel.x = 0.0;
		ptcl[0].vel.y = 0.0;
		ptcl[0].vel.z = 0.0;
		ptcl[0].rad = 0.1;
		ptcl[0].mat = Basalt;
		ptcl[0].mass = 4.0 * M_PI / 3.0 * pow(ptcl[0].rad, 3) * ptcl[0].mat.getDensity();

		ptcl[1].id = 1;
		ptcl[1].pos.x = 0.25;
		ptcl[1].pos.y = 0.0;
		ptcl[1].pos.z = 0.0;
		ptcl[1].vel.x = - 1.0;
		ptcl[1].vel.y = 0.0;
		ptcl[1].vel.z = 0.0;
		ptcl[1].rad = 0.1;
		ptcl[1].mat = Basalt;
		ptcl[1].mass = 4.0 * M_PI / 3.0 * pow(ptcl[1].rad, 3) * ptcl[1].mat.getDensity();
	}
	static void externalForce(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
	}
};

