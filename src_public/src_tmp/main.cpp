#include <particle_simulator.hpp>

class Force{
	public:
	PS::F64vec acc;
	PS::F64vec tor;
	PS::F64 dt;
	void clear(){
		acc = tor = 0;
		dt = 1.0e+30;
	}
};

class Material{
	//bulk modulus, shear modulus and reference density
	PS::F64 bulk, shear, dens;
	public:
	Material(): bulk(0.0), shear(0.0), dens(0.0){
	}
	Material(const PS::F64 bulk_, const PS::F64 shear_, const PS::F64 dens_): bulk(bulk_), shear(shear_), dens(dens_){
	}
	PS::F64 getBulkModulus() const{
		return bulk;
	}
	PS::F64 getShearModulus() const{
		return shear;
	}
	PS::F64 getDensity() const{
		return dens;
	}
	PS::F64 getYoungModulus() const{
		return 9.0 * shear * bulk / (3.0 * bulk + shear);
	}
	PS::F64 getPoissonRatio() const{
		return (3.0 * bulk - 2.0 * shear) / (6.0 * bulk + 2.0 * shear);
	}
};
Material Basalt(26.70e+9, 22.7e+9, 2700.0);
Material Ice   ( 9.47e+9,  2.8e+9,  917.0);

class FP{
	public:
	//id
	PS::S64    id;
	//mass
	PS::F64    mass;
	//inertia
	PS::F64    iner;
	//position
	PS::F64vec pos;
	//velocity
	PS::F64vec vel, vel_pred;
	//acceleration
	PS::F64vec acc;
	//torque
	PS::F64vec tor;
	//radius
	PS::F64    rad;
	//timestep
	PS::F64    dt;
	//material parameter
	Material   mat;

	PS::F64vec getPos() const{
		return pos;
	}
	PS::F64 getCharge() const{
		return mass;
	}
	PS::F64 getRSearch() const{
		return 2.0 * rad;
	}
	void setPos(const PS::F64vec& pos){
		this->pos = pos;
	}
	void copyFromForce(const Force& force){
		this->acc = force.acc;
		this->tor = force.tor;
		this->dt  = force.dt;
	}
	void copyFromFP(const FP& fp){
		*this = fp;
	}
	void writeAscii(FILE* fp) const{
		fprintf(fp, "%lld\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n", id, mass, rad, pos.x, pos.y, pos.z, vel.x, vel.y, vel.z, acc.x, acc.y, acc.z);
	}
	//Euler
	void update(const PS::F64 dt){
		pos += vel * dt;
		vel += acc * dt;
	}
	//KDK
	void kick(const PS::F64 dt){
		vel_pred = vel + 0.5 * acc * dt;
	}
	void drift(const PS::F64 dt){
		pos = pos + vel_pred * dt;
	}
	void kick2(const PS::F64 dt){
		vel = vel_pred + 0.5 * acc * dt;
	}
};

class CollisionReactance{
	const static PS::F64 cn = 0.3;
	public:
	void operator () (const FP* const ep_i, const PS::S32 Nip, const FP* const ep_j, const PS::S32 Njp, Force* const force){
		for(PS::S32 i = 0 ; i < Nip ; ++ i){
			const FP& ith = ep_i[i];
			for(PS::S32 j = 0 ; j < Njp ; ++ j){
				const FP& jth = ep_j[j];
				const PS::F64vec dr = jth.pos - ith.pos;
				const PS::F64vec dv = jth.vel - ith.vel;
				const PS::F64    pd = ith.rad + jth.rad - sqrt(dr * dr);
				if(pd < 0 || dr * dr <= 0.0) continue;
				const PS::F64vec n = dr / sqrt(dr * dr);

				const PS::F64 Eij = 1.0 / ((1.0 - pow(ith.mat.getPoissonRatio(), 2)) / ith.mat.getYoungModulus() + (1.0 - pow(jth.mat.getPoissonRatio(), 2)) / jth.mat.getYoungModulus());
				const PS::F64 rij = 1.0 / (1.0 / ith.rad + 1.0 / jth.rad);
				const PS::F64 mij = 1.0 / (1.0 / ith.mass + 1.0 / jth.mass);
				//replusive normal force
				const PS::F64 knr = 4.0 / 3.0 * Eij * sqrt(rij);
				const PS::F64vec Fnr = - knr * powf(pd, 1.5) * n;
				//damp normal force
				const PS::F64vec Fnd = - cn * sqrt(6.0 * mij * Eij * sqrt(rij * pd)) * (dv * n) * n;
				//
				force[i].acc += Fnr /* + Fnd */;
				force[i].dt  = std::min(M_PI / 50.0 * sqrt(mij / knr), force[i].dt);
			}
			force[i].acc /= ith.mass;
			force[i].tor /= ith.mass;
		}
	}
};

template <class ThisPtcl> double GetGlobalTimestep(const PS::ParticleSystem<ThisPtcl>& ptcl){
	PS::F64 dt = ptcl[0].dt;
	for(PS::S64 i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		dt = std::min(ptcl[i].dt, dt);
		dt = std::min(0.1 * ptcl[i].rad / sqrt(ptcl[i].vel * ptcl[i].vel), dt);
	}
	return PS::Comm::getMinValue(dt);
}

struct system_t{
	PS::F64 time, dt, end_time;
	unsigned int step;
	system_t() : time(0.0), step(0), end_time(0.0), dt(1.0e+30){
	}
};

template <class ThisPtcl> void SetIC(PS::ParticleSystem<FP>& ptcl, PS::DomainInfo& dinfo, system_t& sysinfo){
	ptcl.setNumberOfParticleLocal(2);
	//dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XYZ);
	//dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0, 0.0), PS::F64vec(1.0, 1.0, 1.0));
	sysinfo.end_time = 0.5;
	if(PS::Comm::getRank() != 0) return;
	ptcl[0].id = 0;
	ptcl[0].pos.x = 0.0;
	ptcl[0].pos.y = 0.0;
	ptcl[0].pos.z = 0.0;
	ptcl[0].vel.x = 0.0;
	ptcl[0].vel.y = 0.0;
	ptcl[0].vel.z = 0.0;
	ptcl[0].rad = 0.1;
	ptcl[0].mat = Basalt;
	ptcl[0].mass = 4.0 * M_PI / 3.0 * pow(ptcl[0].rad, 3) * ptcl[0].mat.getDensity();

	ptcl[1].id = 1;
	ptcl[1].pos.x = 0.25;
	ptcl[1].pos.y = 0.0;
	ptcl[1].pos.z = 0.0;
	ptcl[1].vel.x = - 1.0;
	ptcl[1].vel.y = 0.0;
	ptcl[1].vel.z = 0.0;
	ptcl[1].rad = 0.1;
	ptcl[1].mat = Ice;
	ptcl[1].mass = 4.0 * M_PI / 3.0 * pow(ptcl[1].rad, 3) * ptcl[1].mat.getDensity();
}

template <class ThisPtcl> void OutputFileWithTimeInterval(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
	const int NumberOfSnapshot = 50;

	static PS::F64 time = sysinfo.time;
	static PS::S64 step = sysinfo.step;
	if(sysinfo.time >= time){
		char filename[256];
		sprintf(filename, "result/%05d", step);
		ptcl.writeParticleAscii(filename, "%s_%05d_%05d.dat");
		if(PS::Comm::getRank() == 0){
			std::cout << "output " << filename << "." << std::endl;
		}
		time += sysinfo.end_time / NumberOfSnapshot;
		++ step;
	}
}

int main(int argc, char *argv[]){
	PS::Initialize(argc, argv);
	PS::ParticleSystem<FP> ptcl;
	ptcl.initialize();
	PS::TreeForForceShort<Force, FP, FP>::Symmetry tree_coll;
	PS::DomainInfo dinfo;
	dinfo.initialize();

	system_t sysinfo;
	SetIC<FP>(ptcl, dinfo, sysinfo);
	tree_coll.initialize(ptcl.getNumberOfParticleLocal());

	dinfo.decomposeDomainAll(ptcl);
	ptcl.exchangeParticle(dinfo);
	tree_coll.calcForceAllAndWriteBack(CollisionReactance(), ptcl, dinfo);

	for(sysinfo.time = 0, sysinfo.step = 0 ; sysinfo.time < sysinfo.end_time ; sysinfo.time += sysinfo.dt, ++ sysinfo.step){
		dinfo.decomposeDomainAll(ptcl);
		ptcl.exchangeParticle(dinfo);
		sysinfo.dt = GetGlobalTimestep<FP>(ptcl);
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			ptcl[i].kick(sysinfo.dt);
			ptcl[i].drift(sysinfo.dt);
		}
		tree_coll.calcForceAllAndWriteBack(CollisionReactance(), ptcl, dinfo);
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			ptcl[i].kick2(sysinfo.dt);
		}
		//for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i) ptcl[i].update(sysinfo.dt);
		ptcl.adjustPositionIntoRootDomain(dinfo);
		if(PS::Comm::getRank() == 0) std::cout << "time = " << sysinfo.time << " (dt = " << sysinfo.dt << ")" << std::endl;
		OutputFileWithTimeInterval<FP>(ptcl, sysinfo);
	}
	
	PS::Finalize();
	return 0;
}

