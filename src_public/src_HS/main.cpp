#include <particle_simulator.hpp>

class Force{
	public:
	PS::F64vec pos;
	PS::F64vec vel;
	void clear(){
		pos = vel = 0;
	}
};

class FP{
	public:
	PS::S64    id;
	PS::F64    mass;
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64    rad;

	PS::F64vec getPos() const{
		return pos;
	}
	PS::F64 getCharge() const{
		return mass;
	}
	PS::F64 getRSearch() const{
		return 2.0 * rad;
	}

	void setPos(const PS::F64vec& pos){
		this->pos = pos;
	}

	void copyFromForce(const Force& diff){
		pos += diff.pos;
		vel += diff.vel;
	}
	void copyFromFP(const FP& fp){
		*this = fp;
	}
	void writeAscii(FILE* fp) const{
		fprintf(fp, "%lld\t%g\t%g\t%g\t%g\t%g\t%g\n", id, mass, rad, pos.x, pos.y, vel.x, vel.y);
	}
};

class CollisionReactance{
	static const PS::F64 eps_n = 0.1;
	public:
	void operator () (const FP* const ep_i, const PS::S32 Nip, const FP* const ep_j, const PS::S32 Njp, Force* const diff){
		for(PS::S32 i = 0 ; i < Nip ; ++ i){
			const FP& ith = ep_i[i];
			for(PS::S32 j = 0 ; j < Njp ; ++ j){
				const FP& jth = ep_j[j];
				const PS::F64vec dr = jth.pos - ith.pos;
				const PS::F64vec dv = jth.vel - ith.vel;
				const PS::F64    pd = ith.rad + jth.rad - sqrt(dr * dr);
				if(pd < 0 || dr * dr <= 0.0) continue;
				const PS::F64vec n = dr / sqrt(dr * dr);
				diff[i].pos += - jth.mass / (ith.mass + jth.mass) * pd * n;
				diff[i].vel += (1.0 + eps_n) * jth.mass / (ith.mass + jth.mass) * (dv * n) * n;
			}
		}
	}
};

template <class ThisPtcl> double GetGlobalTimestep(const PS::ParticleSystem<ThisPtcl>& ptcl){
	return 1./128.;
}

struct system_t{
	PS::F64 time, dt, end_time;
	unsigned int step;
	system_t() : time(0.0), step(0), end_time(0.0), dt(1.0e+30){
	}
};

template <class ThisPtcl> void SetIC(PS::ParticleSystem<FP>& ptcl, PS::DomainInfo& dinfo, system_t& sysinfo){
	ptcl.setNumberOfParticleLocal(0);
	//dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XYZ);
	dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XY);
	//dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0, 0.0), PS::F64vec(1.0, 1.0, 1.0));
	dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(1.0, 1.0));
	sysinfo.end_time = 1.0;
	if(PS::Comm::getRank() != 0) return;
	ptcl.setNumberOfParticleLocal(256);
	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		ptcl[i].pos.x = rand()/(double)(RAND_MAX);
		ptcl[i].pos.y = rand()/(double)(RAND_MAX);
		//ptcl[i].pos.z = rand()/(double)(RAND_MAX);
		ptcl[i].vel.x = 1.0 - 2.0 * rand()/(double)(RAND_MAX);
		ptcl[i].vel.y = 1.0 - 2.0 * rand()/(double)(RAND_MAX);
		//ptcl[i].vel.z = 1.0 - 2.0 * rand()/(double)(RAND_MAX);
		ptcl[i].vel *= 0.1;
		ptcl[i].mass = 1.0 / ptcl.getNumberOfParticleLocal();
		ptcl[i].rad = 0.01;
	}
}

template <class ThisPtcl> void OutputFileWithTimeInterval(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
	const int NumberOfSnapshot = 10;

	static PS::F64 time = sysinfo.time;
	static PS::S64 step = sysinfo.step;
	if(sysinfo.time >= time){
		char filename[256];
		sprintf(filename, "result/%05d", step);
		ptcl.writeParticleAscii(filename, "%s_%05d_%05d.dat");
		if(PS::Comm::getRank() == 0){
			std::cout << "output " << filename << "." << std::endl;
		}
		time += sysinfo.end_time / NumberOfSnapshot;
		++ step;
	}
}

int main(int argc, char *argv[]){
	PS::Initialize(argc, argv);
	PS::ParticleSystem<FP> ptcl;
	ptcl.initialize();
	PS::TreeForForceShort<Force, FP, FP>::Symmetry tree_coll;
	PS::DomainInfo dinfo;
	dinfo.initialize();

	system_t sysinfo;
	SetIC<FP>(ptcl, dinfo, sysinfo);
	tree_coll.initialize(ptcl.getNumberOfParticleLocal());

	for(sysinfo.time = 0, sysinfo.step = 0 ; sysinfo.time < sysinfo.end_time ; sysinfo.time += sysinfo.dt, ++ sysinfo.step){
		dinfo.decomposeDomainAll(ptcl);
		ptcl.exchangeParticle(dinfo);
		sysinfo.dt = GetGlobalTimestep<FP>(ptcl);
		tree_coll.calcForceAllAndWriteBack(CollisionReactance(), ptcl, dinfo);
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i) ptcl[i].pos += ptcl[i].vel * sysinfo.dt;
		ptcl.adjustPositionIntoRootDomain(dinfo);
		if(PS::Comm::getRank() == 0) std::cout << "time = " << sysinfo.time << " (dt = " << sysinfo.dt << ")" << std::endl;
		OutputFileWithTimeInterval<FP>(ptcl, sysinfo);
	}
	
	PS::Finalize();
	return 0;
}

