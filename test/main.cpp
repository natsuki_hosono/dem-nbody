#include <particle_simulator.hpp>

#include "class.h"

int main(int argc, char *argv[]){
	PS::Initialize(argc, argv);
	PS::ParticleSystem<FP> ptcl;
	ptcl.initialize();

	PS::TreeForForceShort<FP, FP, FP>::Symmetry tree_coll;
	PS::DomainInfo dinfo;
	dinfo.initialize();

	system_t sysinfo;

	dinfo.decomposeDomainAll(ptcl);
	ptcl.exchangeParticle(dinfo);

	PS::Finalize();
	return 0;
}

