#pragma once
class Plane{
	//f(x, y, z) = 0 <-> a * x + b * y + c * z + d = 0;
	const PS::F64 a, b, c, d;
	const Material mat;
	static const PS::F64 cn = 0.3;
	PS::F64 value(const PS::F64vec r) const{
		return a * r.x + b * r.y + c * r.z + d;
	}
	PS::F64vec normal() const{
		return PS::F64vec(a, b, c) / sqrt(a*a + b*b + c*c);
	}
	PS::F64 distance(const PS::F64vec r) const{
		return std::abs(value(r)) / sqrt(a*a + b*b + c*c);
	}
	public:
	Plane(const PS::F64 a_, const PS::F64 b_, const PS::F64 c_, const PS::F64 d_, const Material mat_) : a(a_), b(b_), c(c_), d(d_), mat(mat_){
	}
	PS::F64vec Force(const FP& ith) const{
		const PS::F64vec n = (value(ith.pos) < 0 ? -1.0 : 1.0) * normal();
		//particle-to-surface vectors
		const PS::F64vec dr  = - (ith.pos * n) * n;
		const PS::F64vec dv  = - (ith.vel * n) * n;
		const PS::F64    pd  = ith.rad - distance(ith.pos);
		if(pd < 0) return PS::F64vec(0.0, 0.0, 0.0);
		const PS::F64    Eij = 1.0 / ((1.0 - pow(ith.mat.getPoissonRatio(), 2)) / ith.mat.getYoungModulus() + (1.0 - pow(mat.getPoissonRatio(), 2)) / mat.getYoungModulus());
		const PS::F64    rij = ith.rad;
		const PS::F64    mij = ith.mass;
		const PS::F64    knr = 4.0 / 3.0 * Eij * sqrt(rij);
		const PS::F64vec Fnr = knr * powf(pd, 1.5) * n;
		const PS::F64vec Fnd = cn * sqrt(6.0 * mij * Eij * sqrt(rij * pd)) * (dv * n) * n;
		return (Fnr + Fnd) / ith.mass;
	}
};

