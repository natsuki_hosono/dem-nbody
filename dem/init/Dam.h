#pragma once

template <class ThisPtcl> class Problem{
	public:
	static void SetIC(PS::ParticleSystem<ThisPtcl>& ptcl, PS::DomainInfo& dinfo, system_t& sysinfo){
		std::vector<ThisPtcl> tmp;
		const double dx = 1.0/16.0;
		sysinfo.end_time = 5.0;
		if(PS::Comm::getRank() != 0) return;
		int cnt = 0;
		for(double x = 0.5 * dx ; x < 0.5 ; x += dx){
			for(double y = 0.5 + 0.5 * dx ; y < 1.0 ; y += dx){
				for(double z = 0.5 * dx ; z < 1.0 ; z += dx){
					ThisPtcl ith;
					ith.id = cnt;
					ith.pos.x = x;
					ith.pos.y = y;
					ith.pos.z = z;
					ith.rad = 0.5 * dx;
					ith.mat = Test;
					ith.mass = 4.0 * M_PI / 3.0 * pow(ith.rad, 3) * ith.mat.getDensity();
					tmp.push_back(ith);
					++ cnt;
				}
			}
		}

		ThisPtcl ith;
		ith.pos.x = 0.25;
		ith.pos.y = 0.75;
		ith.pos.z = 1.1;
		ith.rad = 0.5 * dx;
		ith.mat = Test;
		ith.mass = 4.0 * M_PI / 3.0 * pow(ith.rad, 3) * ith.mat.getDensity();
		tmp.push_back(ith);
		

		ptcl.setNumberOfParticleLocal(tmp.size());
		for(int i = 0 ; i < tmp.size() ; ++ i){
			ptcl[i] = tmp[i];
		}
	}
	static void externalForce(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
		Plane floor(0, 0, 1, 0.0, Basalt);
		Plane wall1(1, 0, 0,  0.0, Basalt);
		Plane wall2(1, 0, 0, -1.0, Basalt);
		Plane wall3(0, 1, 0,  0.0, Basalt);
		Plane wall4(0, 1, 0, -1.0, Basalt);
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			//Earth's gravity
			ptcl[i].acc.z -= 9.8;
			ptcl[i].acc += floor.Force(ptcl[i]);
			ptcl[i].acc += wall1.Force(ptcl[i]);
			ptcl[i].acc += wall2.Force(ptcl[i]);
			ptcl[i].acc += wall3.Force(ptcl[i]);
			ptcl[i].acc += wall4.Force(ptcl[i]);
		}
	}
	static void postTimestep(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
	}
};

