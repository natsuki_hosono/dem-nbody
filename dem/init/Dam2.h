#pragma once

template <class ThisPtcl> class Problem{
	public:
	static void SetIC(PS::ParticleSystem<ThisPtcl>& ptcl, PS::DomainInfo& dinfo, system_t& sysinfo){
		sysinfo.end_time = 60.0;
		ptcl.setNumberOfParticleLocal(0);
		if(PS::Comm::getRank() != 0) return;
		ptcl.setNumberOfParticleLocal(1);
		const PS::F64 rad = 0.05;
		
		ThisPtcl ith;
		ith.pos.x = (rand() / double(RAND_MAX)) * (1.0 - 2.0 * rad) + rad;
		ith.pos.y = (rand() / double(RAND_MAX)) * (1.0 - 2.0 * rad) + rad;
		ith.pos.z = 1.0;
		ith.rad   = rad;
		ith.mat   = Test;
		ith.mass = 4.0 * M_PI / 3.0 * pow(ith.rad, 3) * ith.mat.getDensity();
		ptcl[0] = ith;
		
	}
	static void externalForce(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
		const PS::F64 duration = 50.0;
		if(sysinfo.time >= duration){
			Plane floor(0, 0, 1, 0.0, Basalt);
			Plane wall1(1, 0, 0,  0.0, Basalt);
			Plane wall2(1, 0, 0, -2.0, Basalt);
			Plane wall3(0, 1, 0,  0.0, Basalt);
			Plane wall4(0, 1, 0, -1.0, Basalt);
			for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
				//Earth's gravity
				ptcl[i].acc.z -= 9.8;
				ptcl[i].acc += floor.Force(ptcl[i]);
				ptcl[i].acc += wall1.Force(ptcl[i]);
				ptcl[i].acc += wall2.Force(ptcl[i]);
				ptcl[i].acc += wall3.Force(ptcl[i]);
				ptcl[i].acc += wall4.Force(ptcl[i]);
			}
		}else{
			Plane floor(0, 0, 1, 0.0, Basalt);
			Plane wall1(1, 0, 0,  0.0, Basalt);
			Plane wall2(1, 0, 0, -1.0, Basalt);
			Plane wall3(0, 1, 0,  0.0, Basalt);
			Plane wall4(0, 1, 0, -1.0, Basalt);
			for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
				//Earth's gravity
				ptcl[i].acc.z -= 9.8;
				ptcl[i].acc += floor.Force(ptcl[i]);
				ptcl[i].acc += wall1.Force(ptcl[i]);
				ptcl[i].acc += wall2.Force(ptcl[i]);
				ptcl[i].acc += wall3.Force(ptcl[i]);
				ptcl[i].acc += wall4.Force(ptcl[i]);
			}
		}
	}
	static void postTimestep(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
		if(PS::Comm::getRank() != 0) return;
		//inflow velocity [m/sec]
		const PS::F64vec inflow_vel = PS::F64vec(1.0, 0.0, 0.0);
		//radius
		const PS::F64 rad = 0.05;
		//inflow span [sec]
		const PS::F64 inflow_span = 2.0 * sqrt(rad / 9.8);
		//inflow duration
		const PS::F64 duration = 40.0;

		if(sysinfo.time >= duration) return;
		static PS::F64 time = inflow_span;
		
		if(sysinfo.time >= time){
			const std::size_t inflow_number = 4;
			const std::size_t tail = ptcl.getNumberOfParticleLocal() - 1;
			ptcl.setNumberOfParticleLocal(ptcl.getNumberOfParticleLocal() + inflow_number);
			//
			ThisPtcl ith;
			{
				const PS::F64 min_x = 0.0;
				const PS::F64 max_x = 0.5;
				const PS::F64 min_y = 0.0;
				const PS::F64 max_y = 0.5;
				ith.pos.x = (rand() / double(RAND_MAX)) * (max_x - 2.0 * rad) * (max_x - min_x) + (rad + min_x);
				ith.pos.y = (rand() / double(RAND_MAX)) * (max_y - 2.0 * rad) * (max_y - min_y) + (rad + min_y);
				ith.pos.z = 1.0;
				ith.rad   = rad;
				ith.mat   = Test;
				ith.mass  = 4.0 * M_PI / 3.0 * pow(ith.rad, 3) * ith.mat.getDensity();
				ptcl[tail + 1] = ith;
			}
			{
				const PS::F64 min_x = 0.0;
				const PS::F64 max_x = 0.5;
				const PS::F64 min_y = 0.5;
				const PS::F64 max_y = 1.0;
				ith.pos.x = (rand() / double(RAND_MAX)) * (max_x - 2.0 * rad) * (max_x - min_x) + (rad + min_x);
				ith.pos.y = (rand() / double(RAND_MAX)) * (max_y - 2.0 * rad) * (max_y - min_y) + (rad + min_y);
				ith.pos.z = 1.0;
				ith.rad   = rad;
				ith.mat   = Test;
				ith.mass  = 4.0 * M_PI / 3.0 * pow(ith.rad, 3) * ith.mat.getDensity();
				ptcl[tail + 2] = ith;
			}
			{
				const PS::F64 min_x = 0.5;
				const PS::F64 max_x = 1.0;
				const PS::F64 min_y = 0.0;
				const PS::F64 max_y = 0.5;
				ith.pos.x = (rand() / double(RAND_MAX)) * (max_x - 2.0 * rad) * (max_x - min_x) + (rad + min_x);
				ith.pos.y = (rand() / double(RAND_MAX)) * (max_y - 2.0 * rad) * (max_y - min_y) + (rad + min_y);
				ith.pos.z = 1.0;
				ith.rad   = rad;
				ith.mat   = Test;
				ith.mass  = 4.0 * M_PI / 3.0 * pow(ith.rad, 3) * ith.mat.getDensity();
				ptcl[tail + 3] = ith;
			}
			{
				const PS::F64 min_x = 0.5;
				const PS::F64 max_x = 1.0;
				const PS::F64 min_y = 0.5;
				const PS::F64 max_y = 1.0;
				ith.pos.x = (rand() / double(RAND_MAX)) * (max_x - 2.0 * rad) * (max_x - min_x) + (rad + min_x);
				ith.pos.y = (rand() / double(RAND_MAX)) * (max_y - 2.0 * rad) * (max_y - min_y) + (rad + min_y);
				ith.pos.z = 1.0;
				ith.rad   = rad;
				ith.mat   = Test;
				ith.mass  = 4.0 * M_PI / 3.0 * pow(ith.rad, 3) * ith.mat.getDensity();
				ptcl[tail + 4] = ith;
			}

			time += inflow_span;
		}
	}
};

